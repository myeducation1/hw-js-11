//Створи клас, який буде створювати користувачів з ім'ям та прізвищем. 
//Додати до класу метод для виведення імені та прізвища
let divs = document.getElementById('neme');
class NewUser {
	constructor(options) {
       this.firsName = options.firsName;
       this.twoName = options.twoName;
	}
	ifoUser(){
		divs.textContent = this.firsName + " " + this.twoName;
	}
}

let yuriy = new NewUser ({firsName : "Yuriy", twoName : "Svitelskiy"})
yuriy.ifoUser()

//Створи список, що складається з 4 аркушів. 
//Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, 
//а 3 червоний

let twoLi = document.getElementById('two');
twoLi.nextSibling.classList.add("red");
twoLi.previousSibling.classList.add("blue");

//Створи див висотою 400 пікселів і додай на нього подію наведення мишки. 
//При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

let cordinate = document.getElementById("cordenate");
cordinate.addEventListener("mousemove", (e) => cordinate.textContent = "Подія onmouse X:" + e.offsetX + " Y:" + e.offsetY);

//Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута

let firsButton = document.getElementById("button1");
let twoButton = document.getElementById("button2");
let thereButton = document.getElementById("button3");
let foreButton = document.getElementById("button4");
let displeyInfo = document.getElementById("displauy-info");

firsButton.addEventListener("click", (e) => displeyInfo.textContent = "Натиснута кнопка 1");
twoButton.addEventListener("click", (e) => displeyInfo.textContent = "Натиснута кнопка 2");
thereButton.addEventListener("click", (e) => displeyInfo.textContent = "Натиснута кнопка 3");
foreButton.addEventListener("click", (e) => displeyInfo.textContent = "Натиснута кнопка 4");

//Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці

let divRotete = document.getElementById("roteition");
let divRoteteStatus = 1;
let addedDiv = document.getElementById("roteitio-block");

divRotete.addEventListener("mousemove", (e) => {
	if(divRoteteStatus === 1){
		addedDiv.removeChild(divRotete);
        divs.appendChild(divRotete);
        return divRoteteStatus = 0
	}else if (divRoteteStatus === 0){
        divs.removeChild(divRotete);
        addedDiv.appendChild(divRotete);
        return divRoteteStatus = 1
	}
});

//Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

let inputets = document.getElementById("inputets");

inputets.addEventListener("keyup", () => console.log(inputets.value));

//Створіть поле для введення даних у полі введення даних виведіть текст під полем

let inputetsTwo = document.getElementById("inputets2");
let infoInput = document.getElementById("info-input");

inputetsTwo.addEventListener("keyup", () => infoInput.textContent = inputetsTwo.value);